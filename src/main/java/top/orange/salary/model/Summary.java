package top.orange.salary.model;

import top.orange.salary.extend.SummaryDataProxy;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.ReferenceTableType;
import xyz.erupt.jpa.model.BaseModel;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author dell
 */
@Erupt(
        name = "工资管理",
        desc = "工资管理页面",
        orderBy = "id desc",
        dataProxy = SummaryDataProxy.class,
        power = @Power(importable = true, export = true)
)
@Table(name = "s_summary")
@Entity
public class Summary extends BaseModel {

    @ManyToOne
    @EruptField(
            views = @View(
                    title = "员工",
                    column = "name",
                    width = "6%"
            ),
            edit = @Edit(title = "员工",
                    notNull = true,
                    type = EditType.REFERENCE_TABLE,
                    referenceTableType = @ReferenceTableType
            )
    )
    private Staff staff;

    @Transient
    @EruptField(
            views = @View(title = "月薪")
    )
    private Integer dailyWage;

    @Transient
    @EruptField(
            views = @View(title = "工龄工资")
    )
    private Integer wageOneView;

    @Transient
    @EruptField(
            views = @View(title = "加班工资")
    )
    private Integer wageTwoView;

    @EruptField(
            views = @View(title = "加班时长",show = false),
            edit = @Edit(title = "加班时长")
    )
    private Integer wageTwoEdit;

    @Transient
    @EruptField(
            views = @View(title = "奖金")
    )
    private Integer wageThreeView;

    @EruptField(
            views = @View(title = "奖金倍率",show = false),
            edit = @Edit(title = "奖金倍率")
    )
    private Integer wageThreeEdit;

    @Transient
    @EruptField(
            views = @View(title = "迟到扣款")
    )
    private Integer deductionsOneView;

    @EruptField(
            views = @View(title = "迟到次数",show = false),
            edit = @Edit(title = "迟到次数")
    )
    private Integer deductionsOneEdit;

    @Transient
    @EruptField(
            views = @View(title = "旷工扣款")
    )
    private Integer deductionsTwoView;

    @EruptField(
            views = @View(title = "旷工次数",show = false),
            edit = @Edit(title = "旷工次数")
    )
    private Integer deductionsTwoEdit;

    @Transient
    @EruptField(
            views = @View(title = "其它扣款")
    )
    private Integer deductionsThreeView;

    @EruptField(
            views = @View(title = "其它倍率",show = false),
            edit = @Edit(title = "其它倍率")
    )
    private Integer deductionsThreeEdit;

    @Transient
    @EruptField(
            views = @View(title = "最终工资")
    )
    private Integer summary;

    public Summary() {
    }

    public Summary(Staff staff) {
        this.staff = staff;
    }

    public Long getStaffId(){
        return staff.getId();
    }
}
