package top.orange.salary.model;

import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.NumberType;
import xyz.erupt.upms.handler.DictCodeChoiceFetchHandler;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * @author dell
 */
@Erupt(
        name = "奖金项管理",
        desc = "公司奖金管理页面",
        orderBy = "id desc",
        power = @Power(importable = true, export = true)
)
@Table(name = "s_wage")
@Entity
public class Wage extends HyperModelVo {

    @EruptField(
            views = @View(title = "奖金项目名称"),
            edit = @Edit(title = "奖金项目名称",
                    notNull = true,
                    type = EditType.CHOICE,
                    choiceType = @ChoiceType(
                            fetchHandler = DictCodeChoiceFetchHandler.class,
                            fetchHandlerParams = {"wages", "5000"}
                    )
            )
    )
    private String name;

    @EruptField(
            views = @View(title = "金额"),
            edit = @Edit(title = "金额",
                    notNull = true,
                    numberType = @NumberType(min = 0)
            )
    )
    private Integer amount;

    @Lob
    @EruptField(
            views = @View(
                    title = "备注",
                    type = ViewType.HTML,
                    export = false
            ),
            edit = @Edit(
                    title = "备注",
                    type = EditType.HTML_EDITOR
            )
    )
    private String remarks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
