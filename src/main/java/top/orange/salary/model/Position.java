package top.orange.salary.model;

import top.orange.salary.extend.PositionDataProxy;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.ChoiceType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.annotation.sub_field.sub_edit.SliderType;
import xyz.erupt.upms.handler.DictCodeChoiceFetchHandler;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * @author dell
 */
@Erupt(
        name = "职位管理",
        desc = "员工职位页面",
        orderBy = "id desc",
        dataProxy = PositionDataProxy.class,
        power = @Power(importable = true, export = true)
)
@Table(name = "s_position")
@Entity
public class Position extends HyperModelVo {

    @EruptField(
        views = @View(title = "职位名称"),
        edit = @Edit(title = "职位名称",
                notNull = true,
                search = @Search(vague = true)
        )
    )
    private String name;

    @EruptField(
            views = @View(
                    title = "编码"
            ),
            edit = @Edit(
                    title = "编码",
                    notNull = true,
                    search = @Search(vague = true)
            )
    )
    private String code;

    @EruptField(
        views = @View(title = "所属序列"),
        edit = @Edit(title = "所属序列",
                notNull = true,
                type = EditType.CHOICE,
                choiceType = @ChoiceType(
                        fetchHandler = DictCodeChoiceFetchHandler.class,
                        fetchHandlerParams = {"sequence", "5000"}
                ),
                search = @Search(vague = true)
        )
    )
    private String sequence;

    @EruptField(
        views = @View(title = "序列级别"),
        edit = @Edit(title = "序列级别",
                notNull = true,
                type = EditType.CHOICE,
                choiceType = @ChoiceType(
                        fetchHandler = DictCodeChoiceFetchHandler.class,
                        fetchHandlerParams = {"level", "5000"}
                ),
                search = @Search(vague = true)
        )
    )
    private String level;

    @EruptField(
        views = @View(title = "基础薪资"),
        edit = @Edit(title = "基础薪资",
                notNull = true,
                type = EditType.SLIDER,
                sliderType = @SliderType(
                        min = 1000,
                        max = 1000000,
                        step = 100
                )
        )
    )
    private Integer basiSalary;

    @Lob
    @EruptField(
        views = @View(
                title = "备注",
                type = ViewType.HTML,
                export = false
        ),
        edit = @Edit(
                title = "备注",
                type = EditType.HTML_EDITOR
        )
    )
    private String remarks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getBasiSalary() {
        return basiSalary;
    }

    public void setBasiSalary(Integer basiSalary) {
        this.basiSalary = basiSalary;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
