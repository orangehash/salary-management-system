package top.orange.salary.model;

import top.orange.salary.extend.StaffDataProxy;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.upms.handler.DictCodeChoiceFetchHandler;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author dell
 */
@Erupt(
        name = "员工管理",
        desc = "员工管理页面",
        orderBy = "id desc",
        dataProxy = StaffDataProxy.class,
        power = @Power(importable = true, export = true)
)
@Table(name = "s_staff")
@Entity
public class Staff extends HyperModelVo {

    @EruptField(
        views = @View(title = "员工工号"),
        edit = @Edit(
                title = "员工工号",
                notNull = true,
                search = @Search(vague = true)
        )
    )
    private String number;

    @EruptField(
        views = @View(title = "员工名称"),
        edit = @Edit(
                title = "员工名称",
                notNull = true,
                search = @Search(vague = true)
        )
    )
    private String name;

    @EruptField(
            views = @View(title = "手机号码"),
            edit = @Edit(
                    title = "手机号码",
                    notNull = true
            )
    )
    private String phone;

    @EruptField(
        views = @View(title = "部门"),
        edit = @Edit(title = "部门",
                notNull = true,
                type = EditType.CHOICE,
                choiceType = @ChoiceType(
                        fetchHandler = DictCodeChoiceFetchHandler.class,
                        fetchHandlerParams = {"department", "5000"}
                ),
                search = @Search(vague = true)
        )
    )
    private String department;

    @EruptField(
        views = @View(title = "职位"),
        edit = @Edit(title = "职位",
                notNull = true,
                type = EditType.CHOICE,
                choiceType = @ChoiceType(
                        fetchHandler = DictCodeChoiceFetchHandler.class,
                        fetchHandlerParams = {"position", "5000"}
                ),
                search = @Search(vague = true)
        )
    )
    private String position;

    @EruptField(
        views = @View(title = "入职日期"),
        edit = @Edit(title = "入职日期",
                type = EditType.DATE,
                notNull = true,
                dateType = @DateType(
                        pickerMode = DateType.PickerMode.HISTORY
                )
        )
    )
    private Date entryDate;

    @EruptField(
        views = @View(title = "离职日期"),
        edit = @Edit(title = "离职日期",
                type = EditType.DATE,
                dateType = @DateType(
                        pickerMode = DateType.PickerMode.HISTORY
                )
        )
    )
    private Date resignationDate;

    @EruptField(
        views = @View(title = "月薪"),
        edit = @Edit(title = "月薪",
                notNull = true
        )
    )
    private Integer dailyWage;

    @EruptField(
        views = @View(title = "状态"),
        edit = @Edit(title = "状态",
                notNull = true,
                type = EditType.CHOICE,
                choiceType = @ChoiceType(vl = {
                                @VL(value = "1", label = "在职"),
                                @VL(value = "2", label = "实习"),
                                @VL(value = "3", label = "离职"),
                                @VL(value = "4", label = "开除"),
                        }
                ),
                search = @Search(vague = true)
        )
    )
    private String status;

    @Lob
    @EruptField(
        views = @View(
                title = "备注",
                type = ViewType.HTML,
                export = false
        ),
        edit = @Edit(
                title = "备注",
                type = EditType.HTML_EDITOR
        )
    )
    private String remarks;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public Date getResignationDate() {
        return resignationDate;
    }

    public void setResignationDate(Date resignationDate) {
        this.resignationDate = resignationDate;
    }

    public Integer getDailyWage() {
        return dailyWage;
    }

    public void setDailyWage(Integer dailyWage) {
        this.dailyWage = dailyWage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "Staff{" +
                "number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", department='" + department + '\'' +
                ", position='" + position + '\'' +
                ", entryDate='" + entryDate + '\'' +
                ", resignationDate='" + resignationDate + '\'' +
                ", dailyWage=" + dailyWage +
                ", status='" + status + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
