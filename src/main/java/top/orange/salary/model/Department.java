package top.orange.salary.model;

import top.orange.salary.extend.DepartmentDataProxy;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_erupt.Power;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.ViewType;
import xyz.erupt.annotation.sub_field.sub_edit.ReferenceTableType;
import xyz.erupt.annotation.sub_field.sub_edit.Search;
import xyz.erupt.upms.helper.HyperModelVo;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author dell
 */
@Erupt(
        name = "部门管理",
        desc = "公司部门页面",
        orderBy = "id desc",
        power = @Power(importable = true, export = true),
        dataProxy = DepartmentDataProxy.class
)
@Table(name = "s_department")
@Entity
public class Department extends HyperModelVo {
    @EruptField(
        views = @View(title = "名称"),
        edit = @Edit(title = "名称",
                notNull = true,
                search = @Search(vague = true)
        )
    )
    private String name;

    @EruptField(
            views = @View(
                    title = "编码"
            ),
            edit = @Edit(
                    title = "编码",
                    notNull = true,
                    search = @Search(vague = true)
            )
    )
    private String code;

    @ManyToOne
    @EruptField(
        views = {
                @View(title = "总监", column = "name"),
                @View(title = "手机号",column = "phone")
        },
        edit = @Edit(title = "总监",
                type = EditType.REFERENCE_TABLE,
                referenceTableType = @ReferenceTableType
        )
    )
    private Staff staff;

    @Lob
    @EruptField(
        views = @View(
                title = "性质",
                type = ViewType.HTML
        ),
        edit = @Edit(title = "性质",
                type = EditType.TEXTAREA
        )
    )
    private String effect;

    @Lob
    @EruptField(
        views = @View(
                title = "备注",
                type = ViewType.HTML,
                export = false
        ),
        edit = @Edit(
                title = "备注",
                type = EditType.HTML_EDITOR
        )
    )
    private String remarks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
