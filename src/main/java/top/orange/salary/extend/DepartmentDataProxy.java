package top.orange.salary.extend;

import org.springframework.stereotype.Service;
import top.orange.salary.model.Department;
import xyz.erupt.annotation.fun.DataProxy;
import xyz.erupt.jpa.dao.EruptDao;
import xyz.erupt.upms.model.EruptDict;
import xyz.erupt.upms.model.EruptDictItem;

import javax.annotation.Resource;

/**
 * @author dell
 */
@Service
public class DepartmentDataProxy implements DataProxy<Department> {
    @Resource
    private EruptDao eruptDao;

    @Override
    public void afterAdd(Department department) {
        String code = department.getCode();
        EruptDictItem eruptDictItem1 = eruptDao.queryEntity(EruptDictItem.class, "code = '" + code + "'");
        if (eruptDictItem1 == null){
            EruptDictItem eruptDictItem = new EruptDictItem();
            eruptDictItem.setEruptDict(eruptDao.getEntityManager().find(EruptDict.class, (long) 1));
            eruptDictItem.setCode(code);
            eruptDictItem.setName(department.getName());
            eruptDao.persist(eruptDictItem);
        }
    }

    @Override
    public void afterUpdate(Department department) {
        String code = department.getCode();
        EruptDictItem eruptDictItem = eruptDao.queryEntity(EruptDictItem.class, "code = '" + code + "'");
        eruptDictItem.setCode(code);
        eruptDictItem.setName(department.getName());
        eruptDao.merge(eruptDictItem);
    }

    @Override
    public void afterDelete(Department department) {
        String code = department.getCode();
        EruptDictItem eruptDictItem = eruptDao.queryEntity(EruptDictItem.class, "code = '" + code + "'");
        eruptDao.delete(eruptDictItem);
    }
}
