package top.orange.salary.extend;

import org.springframework.stereotype.Service;
import top.orange.salary.model.Deduction;
import top.orange.salary.model.Staff;
import top.orange.salary.model.Summary;
import top.orange.salary.model.Wage;
import xyz.erupt.annotation.fun.DataProxy;
import xyz.erupt.jpa.dao.EruptDao;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author dell
 */
@Service
public class SummaryDataProxy implements DataProxy<Summary> {

    @Resource
    private EruptDao eruptDao;

    Wage[] wages = new Wage[3];
    Deduction[] deductions = new Deduction[3];
    Staff staff;
    Summary sum;
    int summary;

    @Override
    public void afterFetch(Collection<Map<String, Object>> list) {
        for (int i = 0; i < wages.length; i++) {
            deductions[i] = getEntityManager(Deduction.class, (long) i + 1);
            wages[i] = getEntityManager(Wage.class, (long) i + 1);
        }

        List<Staff> staffs = eruptDao.queryEntityList(Staff.class);
        List<Summary> summaries = eruptDao.queryEntityList(Summary.class);

        for(Map<String, Object> map : list){
            summary = 0;

            for (Summary su : summaries){
                if (map.get("id").equals(su.getId())){
                    sum = su;
                    break;
                }
            }
            for (Staff st : staffs){
                if (sum.getStaffId().equals(st.getId())){
                    staff = st;
                    break;
                }
            }

            //月薪
            int dailyWage = this.staff.getDailyWage();
            summary += dailyWage ;
            map.put("dailyWage",dailyWage);

            //工龄工资
            int wageOne = getWageOne(this.staff.getEntryDate());
            int wageOneView = wages[0].getAmount() * wageOne;
            summary += wageOneView;
            map.put("wageOneView",wageOneView);

            //加班补贴
            Object wageTwoEdit = map.get("wageTwoEdit");
            if (wageTwoEdit != null){
                int wageTwoView = wages[1].getAmount() * (Integer) wageTwoEdit;
                summary += wageTwoView;
                map.put("wageTwoView",wageTwoView);
            }

            //奖金
            Object wageThreeEdit = map.get("wageThreeEdit");
            if (wageThreeEdit != null){
                int wageThreeView = wages[2].getAmount() * (Integer) wageThreeEdit;
                summary += wageThreeView;
                map.put("wageThreeView",wageThreeView);
            }

            //迟到
            putDeductions("deductionsOneView","deductionsOneEdit",map,0);
            //矿工
            putDeductions("deductionsTwoView","deductionsTwoEdit",map,1);
            //其它
            putDeductions("deductionsThreeView","deductionsThreeEdit",map,2);

            map.put("summary",summary);
        }
    }

    /**
     * 计算过去日期与现在日期的日期差值用于计算工龄月份
     * @param entryDate 过去日期
     * @return 月份差值
     */
    private int getWageOne(Date entryDate){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        DateTimeFormatter sdf = DateTimeFormatter.ofPattern("yyyy-MM");
        YearMonth nowMonth = YearMonth.parse(format.format(new Date()), sdf);

        YearMonth beforeMonth = null;
        try {
            beforeMonth = YearMonth.parse(format.format(format.parse(entryDate.toString())), sdf);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int monDif = Objects.requireNonNull(nowMonth).getMonthValue() - Objects.requireNonNull(beforeMonth).getMonthValue();
        int month = (nowMonth.getYear() - beforeMonth.getYear())*12;
        return month+monDif;
    }

    /**
     * @param deductionsView 扣款项的名称
     * @param deductionsEdit 扣款项的值
     * @param map map对象
     * @param i 数组下标
     */
    private void putDeductions(String deductionsView, String deductionsEdit, Map<String, Object> map, int i){
        Object deductionEdit = map.get(deductionsEdit);
        if (deductionEdit != null){
            int deductionView = deductions[i].getAmount() * (Integer) deductionEdit;
            summary -= deductionView;
            map.put(deductionsView,deductionView);
        }
    }

    /**
     * @param entityClass 实体类类对象
     * @param primaryKey 主键id
     * @param <T> 泛型
     * @return 对应主键id的对象
     */
    private <T> T getEntityManager(Class<T> entityClass, Object primaryKey){
        return eruptDao.getEntityManager().find(entityClass,primaryKey);
    }
}
