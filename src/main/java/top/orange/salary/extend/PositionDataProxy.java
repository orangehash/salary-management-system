package top.orange.salary.extend;

import org.springframework.stereotype.Service;
import top.orange.salary.model.Position;
import xyz.erupt.annotation.fun.DataProxy;
import xyz.erupt.jpa.dao.EruptDao;
import xyz.erupt.upms.model.EruptDict;
import xyz.erupt.upms.model.EruptDictItem;

import javax.annotation.Resource;

/**
 * @author dell
 */
@Service
public class PositionDataProxy implements DataProxy<Position> {
    @Resource
    private EruptDao eruptDao;

    @Override
    public void afterAdd(Position position) {
        String code = position.getCode();
        EruptDictItem eruptDictItem1 = eruptDao.queryEntity(EruptDictItem.class, "code = '" + code + "'");
        if (eruptDictItem1 == null){
            EruptDictItem eruptDictItem = new EruptDictItem();
            eruptDictItem.setEruptDict(eruptDao.getEntityManager().find(EruptDict.class, (long) 2));
            eruptDictItem.setCode(code);
            eruptDictItem.setName(position.getName());
            eruptDao.persist(eruptDictItem);
        }
    }

    @Override
    public void afterUpdate(Position position) {
        String code = position.getCode();
        EruptDictItem eruptDictItem = eruptDao.queryEntity(EruptDictItem.class, "code = '" + code + "'");
        eruptDictItem.setCode(code);
        eruptDictItem.setName(position.getName());
        eruptDao.merge(eruptDictItem);
    }

    @Override
    public void afterDelete(Position position) {
        String code = position.getCode();
        EruptDictItem eruptDictItem = eruptDao.queryEntity(EruptDictItem.class, "code = '" + code + "'");
        eruptDao.delete(eruptDictItem);
    }
}
