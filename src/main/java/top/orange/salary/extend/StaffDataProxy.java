package top.orange.salary.extend;

import org.springframework.stereotype.Service;
import top.orange.salary.model.Staff;
import top.orange.salary.model.Summary;
import xyz.erupt.annotation.fun.DataProxy;
import xyz.erupt.jpa.dao.EruptDao;

import javax.annotation.Resource;

/**
 * @author dell
 */
@Service
public class StaffDataProxy implements DataProxy<Staff> {

    @Resource
    private EruptDao eruptDao;

    @Override
    public void afterAdd(Staff staff) {
        eruptDao.persist(new Summary(staff));
    }

    @Override
    public void beforeUpdate(Staff staff) {
        String deleteOne = "4";
        String deleteTwo = "3";
        String insertOne = "1";
        String insertTwo = "2";
        String status = staff.getStatus();
        if (deleteOne.equals(status) || deleteTwo.equals(status)){
            Summary summary = eruptDao.queryEntity(Summary.class, "staff_id = " + staff.getId());
            eruptDao.delete(summary);
        }else if (insertOne.equals(status) || insertTwo.equals(status)){
            Staff oldStaff = eruptDao.getEntityManager().find(Staff.class, staff.getId());
            if (oldStaff == null){
                eruptDao.persist(new Summary(staff));
            }
        }
    }
}
