<h1 align="center"> Salary System &nbsp; 🚀 &nbsp; 企业工资管理系统 </h1>

## 🧐 介绍
本项目是基于[erupt](https://gitee.com/erupt/erupt) 的企业员工工资管理系统 v1.3.0

## 🥏 架构说明
```lua
top.orange.salary
├── extend -- 扩展功能
|   ├── DepartmentDataProxy --公司部门的扩展处理类
|   ├── PositionDataProxy --公司职位的扩展处理类
|   ├── StaffDataProxy --员工管理的扩展处理类
|   └── SummaryDataProxy --工资管理的扩展处理类
├── model -- 核心功能模块
|   ├── Deduction -- 罚金模块基础类
|   ├── Department -- 部门模块基础类
|   ├── Position -- 职位模块基础类
|   ├── Staff -- 员工模块基础类
|   ├── Summary -- 核心工资管理模块基础类
|   └── Wage -- 奖金模块基础类
└── SalaryApplication -- 项目启动类
```
## 🌈 安装教程
前置:jdk1.8 mysql8 redis5.0.7
1.  新建库后导入sql文件
2.  修改application.properties中空白的配置
3.  运行SalaryApplication启动类
3.  管理员账号密码为orange

未配置redis的时候需要设置erupt.redis-session为false

## 📟 演示截图
没有 自己搭一个跑起来看吧